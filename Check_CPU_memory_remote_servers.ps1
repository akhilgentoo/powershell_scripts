﻿
$ServerListFile = "D:\*******"   
$ServerList = Get-Content $ServerListFile -ErrorAction SilentlyContinue  
$Result = @()  
ForEach($computername in $ServerList)  
{ 
 
$AVGProc = Get-WmiObject -computername $computername win32_processor |  
Measure-Object -property LoadPercentage -average | Select average 
$OS = gwmi -Class win32_operatingsystem -computername $computername | 
Select-Object @{Name = "MemoryUsage"; Expression = {“{0:N2}” -f ((($_.TotalVisibleMemorySize - $_.FreePhysicalMemory)*100)/ $_.TotalVisibleMemorySize) }} 
$vol1 = Get-WmiObject -Class win32_Volume -ComputerName $computername -Filter "DriveLetter = 'C:'" | 
Select-object @{Name = "C PercentFree"; Expression = {“{0:N2}” -f  (($_.FreeSpace / $_.Capacity)*100) } } 
$vol2 = Get-WmiObject -Class win32_Volume -ComputerName $computername -Filter "DriveLetter = 'D:'" | 
Select-object @{Name = "D PercentFree"; Expression = {“{0:N3}” -f  (($_.FreeSpace / $_.Capacity)*100) } } 
   
$result += [PSCustomObject] @{  
        ServerName = "$computername" 
        CPULoad = "$($AVGProc.Average)%" 
        MemLoad = "$($OS.MemoryUsage)%" 
        CDrive = "$($vol1.'C PercentFree')%" 
        DDrive = "$($vol2.'D PercentFree')%"
    } 
 
    $Outputreport = "<HTML><TITLE> Server Health Report </TITLE> 
                     <BODY background-color:peachpuff> 
                     <font color =""#99000"" face=""Microsoft Tai le""> 
                     <H2> Server Health Report </H2></font> 
                     <Table border=1 cellpadding=0 cellspacing=0> 
                     <TR bgcolor=gray align=center> 
                       <TD><B>Server Name</B></TD> 
                       <TD><B>Avrg.CPU Utilization</B></TD> 
                       <TD><B>Memory Utilization</B></TD> 
                       <TD><B>C Drive Available Space </B></TD> 
                       <TD><B>D Drive Available Space</B></TD></TR>" 
                         
    Foreach($Entry in $Result)  
     
        {  
          if((($Entry.CpuLoad) -ge 50 -or ($Entry.memload)) -ge "70")  
          {  
            $Outputreport += "<TR bgcolor=White>"  
          }  
          else 
           { 
            $Outputreport += "<TR>"  
          } 
          $Outputreport += "<TD>$($Entry.Servername)</TD><TD align=center>$($Entry.CPULoad)</TD><TD align=center>$($Entry.MemLoad)</TD><TD align=center>$($Entry.Cdrive)</TD><TD align=right>$($Entry.Ddrive)</TD></TR>"
        } 
     $Outputreport += "</Table></BODY></HTML>"  
        }  
  
$Outputreport | out-file D:\*******\CPU_MEMORY.html

  
#Invoke-Expression C:\Scripts\auminonprod_result.D:\Temp\Test\auminonprod_result.html
##Send email functionality from below line, use it if you want    
$From = "HealthCheck <do_not_reply@anthem.com>"
$To = "akhil@gmail.com"
$Attachment = "D:\*******\CPU_MEMORY.html"
$Subject = "HealthCheck"
$curDateTime = Get-Date
$timeStamp = $curDateTime.ToShortDateString() + " " + $curDateTime.ToShortTimeString()
$Body = "JVMs CPU and Memory Status, Please check the logs.. :$timeStamp "
$SMTPServer = "smtp.wellpoint.com"
$SMTPPort = "25"	
Send-MailMessage -From $From -to $To  -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -Attachments $Attachment –DeliveryNotificationOption OnSuccess	