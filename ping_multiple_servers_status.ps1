﻿$file1='D:\*******\SERVER_PING_STATUS.csv'
if (Test-Path $file1 ){
    Clear-Host
    Write-Host "Output File already Exists, deleting output file now - $file1.`n" -ForegroundColor Black -BackgroundColor Green
    Remove-Item $file1
}
$ServerName = Get-Content D:\*******\allservers.txt
$Result = foreach ($Server in $ServerName) { 
    [PSCustomObject]@{
        Server = $Server
        Ping = Test-Connection -ComputerName $Server -Count 1 -Quiet
    }
} 
$Result 
$Result | Export-Csv -Path D:\*******\SERVER_PING_STATUS.csv -NoTypeInformation



$From = "OPS<do_not_reply@anthem.com>"
$To = "akhil@gmail.com"
$Attachment = "D:\*******\SERVER_PING_STATUS.csv"
$Subject = "SERVERS_PING_STATUS"
$Body = "SERVERS_PING_STATUS"
$SMTPServer = "smtp.wellpoint.com"
$SMTPPort = "25"	
	
Send-MailMessage -From $From -to $To -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -Attachments $Attachment –DeliveryNotificationOption OnSuccess