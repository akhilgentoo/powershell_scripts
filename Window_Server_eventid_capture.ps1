$ServerListFile = "D:\*******\PROD.txt"   
$ServerList = Get-Content $ServerListFile -ErrorAction SilentlyContinue  
$Result = @()  
$startTime = Get-Date
$endTime = Get-Date 
$file1='D:\*******\eventid129_status.txt'
$file2='D:\*******\eventid7011_status.txt'
if (Test-Path $file1 ){
    Clear-Host
    Write-Host "Output File already Exists, deleting output file now - $file1.`n" -ForegroundColor Black -BackgroundColor Green
    Remove-Item $file1
     
    
}

if (Test-Path $file2 ){
    Clear-Host
    Write-Host "Output File already Exists, deleting output file now - $file2.`n" -ForegroundColor Black -BackgroundColor Green
    Remove-Item $file2
     
    
}

ForEach($computername in $ServerList)  
{ 

$space = echo `n`n
$data = Get-WinEvent -ComputerName $computername  -LogName System -MaxEvents 50 | Where-Object {  $_.Id -eq '129' -and  $_.TimeCreated -le $startTime }
$computername,$data,$space  >> 'D:\*******\eventid129_status.txt'


$data1 = Get-WinEvent -ComputerName $computername  -LogName System -MaxEvents 50 | Where-Object {  $_.Id -eq '7011' -and  $_.TimeCreated -le $startTime }
$computername,$data1,$space  >> 'D:\*******\eventid7011_status.txt'

#echo `n
#echo $computername
#echo `n
#echo $data 
#echo `n

}
$From = "PROD_Server_EventLog_Status <do_not_reply@anthem.com>"
$To = "akhil@gmail.com"
$Attachment = 'D:\*******\eventid129_status.txt'
$Attachment1 = 'D:\*******\eventid7011_status.txt'
$Subject = "PROD_Server_EventLog_Status"
$Body = "PROD_Server_EventLog_Status"
$SMTPServer = "smtp.wellpoint.com"
$SMTPPort = "25"
Send-MailMessage -From $From -to $To  -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -Attachments $Attachment,$Attachment1  –DeliveryNotificationOption OnSuccess